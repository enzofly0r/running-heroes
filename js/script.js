$(document).ready(function(){
  $("#menu-mobile").hide();
  $("#b-menu-close").hide();
  var menu = 1;
  $('.burger-menu').click(function(){
    menu++;
    $("#menu-mobile").fadeToggle();
    if(menu%2 == 0){
      $(".burger-menu").children('li.b-menu').hide();
      $(".burger-menu").children('li#b-menu-close').show();
    }
    else{
      $(".burger-menu").children('li#b-menu-close').hide();
      $(".burger-menu").children('li.b-menu').show();
    }
  })

})
