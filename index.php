<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Running Heroes: rien ne vous arrête!</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font roboto -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500" rel="stylesheet">
  <!-- My Css -->
  <link rel="stylesheet" href="css/style.min.css">
  <!--  jQuery -->
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <!-- jQueryUI -->
  <script type="text/javascript" src="js/jquery-ui.min.js"></script>
  <!-- font awesome icon -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- My Javascript -->
  <script type="text/javascript" src="js/script.js"></script>
</head>
<body>
  <div id="menu-mobile">
    <ul>
      <a href="#"><li>accueil</li></a>
      <a href="#"><li>lorem 1</li></a>
      <a href="#"><li>jeux</li></a>
      <a href="#"><li>lorem 2</li></a>
      <a href="#"><li>lorem 3</li></a>
      <a href="#"><li>lorem 4</li></a>
    </ul>
  </div>
  <section class="content" id="background"> <!-- section gauche image -->
    <nav>
      <ul></ul>
      <div id="banderole"><h1 class="btn">running heroes</h1></div>
      <ul class="burger-menu">
        <li class="b-menu"></li>
        <li class="b-menu"></li>
        <li class="b-menu"></li>
        <li id="b-menu-close"><i class="fa fa-times fa-2x" aria-hidden="true"></i></li>
        <li>Menu</li>
      </ul>
    </nav>
    <h2>rien ne vous arrête</h2>
  </section>

  <section class="content" id="infos"> <!-- section droite infos -->
    <div class="message">
      <h4 class="text-custom">
        du 14 décembre 2015 au 15 septembre 2016
        <img id="oneday" src="gallery/oneday.png" alt="" />
      </h4>
      <h2 class="text-custom">gagnez</h2>
      <h3 class="text-custom">100 caméras d'action<br> 150 dossards runningheroes<br> et pleins d'autres cadeaux</h3>
      <p>
        Rentrez votre email
      </p>
      <input type="email" name="email" class="input-search" placeholder="e-mail"><br>
      <button type="submit" class="btn" name="button">validez</button>
    </div>
</section>

</body>
</html>
